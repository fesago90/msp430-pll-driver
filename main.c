// PLL Driver
//
// MSP430 interfaces through SPI with an ADF4107 PLL that drives a VCO to generate
// signals in 100 channels spaced 1 MHz apart starting at 5.751 GHz ending at 5.850 GHz
//
// Felipe Salazar, July 2012

#include <msp430g2452.h>
#include "ADF4107_PLL.h"

#define LED1	BIT0		// port 1

#define DWELL_TIME 		10000	// dwell time for each channel, in cycles (at 32768 Hz)
#define LED_BLINK_TIME	500		// blink time to notify channel hop, in cycles (at 32768 Hz)

uint8_t xtalInitMode = 0;
uint8_t pllDriverMode = 0;

void init_pins(void);
void init_xtal(void);
void drive_PLL(void);

void main(void)
{
	// Disable WDT
	WDTCTL 		= WDTPW + WDTHOLD;

	// Set DCO to 1MHz
	BCSCTL1		= CALBC1_1MHZ;		// this line and the one below
	DCOCTL 		= CALDCO_1MHZ;		// set pre-calibrated values for 1MHz

	init_pins();					// init pins as outputs, low

	P1OUT 		|= LED1;
	init_xtal();					// init & wait for xtal stabilization
	P1OUT 		&= ~LED1;

	PLL_init();						// init USI and PLL

	drive_PLL();
}

void drive_PLL(void)
{
	pllDriverMode = 1;

	// set up timer for frequency hops every 10k xtal cycles (~0.3052 s)
	// also, blink on each hop
	TACTL 		|= TACLR;				// reset TimerA
	TACCR2		= DWELL_TIME;			// set compare register2 to dwell time
	TACCTL2		|= CCIE;				// enable compare register2 interrupt
	TACCR1		= LED_BLINK_TIME;		// set compare register1 to blink length
	TACCTL1		|= CCIE;				// enable compare register1 interrupt
	TACTL		|= MC_2;				// start timer

	P1OUT	^= LED1;					// about to take first frequency hop, LED on

	while(1) {
		PLL_step_freq_outward();		// make the hop
		_low_power_mode_3();
	}
}

void init_xtal(void)
{
	unsigned int xtalDelayAttempts = 20;

	// Use 12pF load on xtal oscillator
	BCSCTL3 	|= XCAP_3;

	xtalInitMode = 1;

	// Wait for xtal to stabilize
	_enable_interrupts();			// enable maskable interrupts
	TACCR2 		= 32768 >> 2;		// will wait half a second until interrupt
	TACCTL2 	|= CCIE;			// enable compare interrupt
	TACTL 		|= TASSEL_1 | MC_2;	// use ACLK, start timer (cont. mode)

	while ( (IFG1 & OFIFG) && xtalDelayAttempts > 0 ) {
		xtalDelayAttempts--;
		IFG1 	&= ~OFIFG;			// clear osc fault flag
		TACTL 	|= MC_2;
		_low_power_mode_3();		// go into low power mode and wait
		TACTL	|= TACLR;			// clear timer
	}

	TACTL &= ~(BIT5 | BIT4);		// stop timer (set MC_0)

	xtalInitMode = 0;

	if (xtalDelayAttempts == 0) {	// xtal malfunction
		_low_power_mode_4();		// eternal sleep
	}
}

void init_pins(void)
{
	P1DIR = 0xFF;				// set all pins as output
	P1OUT = 0;				// output low on all pins

	P2DIR = 0xFF;				// same as above
	P2OUT = 0;				// output low on all pins
}

#pragma vector = TIMER0_A1_VECTOR
interrupt void timerA_ISR(void)
{
	switch (TAIV) {
	case 2:				// TACCR1 CCIFG
		P1OUT	^= LED1;
		break;
	case 4:				// TACCR2 CCIFG
		if (xtalInitMode) {

		}

		if (pllDriverMode) {
			TACCR1	= TACCR2 + LED_BLINK_TIME;
			TACCR2 	+= DWELL_TIME;
			P1OUT 	^= LED1;
		}

		_low_power_mode_off_on_exit();

		break;
	}
}

