/*
 * ADF4107_PLL.h
 *
 *  Created on: Jul 11, 2012
 *      Author: Felipe
 */

#ifndef ADF4107_PLL_H_
#define ADF4107_PLL_H_

#include <stdint.h>

#define SCLK	BIT5	// port 1
#define SDO		BIT6	// port 1

#define PLL_POUT P2OUT
#define PLL_PDIR P2DIR

#define CE		BIT0	// used to power down PLL
#define LE		BIT1	// used to initiate SPI operations

#define CE_LO (PLL_POUT &= ~CE)
#define CE_HI (PLL_POUT |= CE)

#define LE_LO (PLL_POUT &= ~LE)
#define LE_HI (PLL_POUT |= LE)

void PLL_init(void);
void PLL_write_latch(uint8_t (*latch)[3]);
void PLL_step_freq_outward(void);


#endif /* ADF4107_PLL_H_ */
