/*
 * ADF4107_PLL.c
 *
 *  Created on: Jul 11, 2012
 *      Author: Felipe
 */
#include <msp430g2452.h>
#include "ADF4107_PLL.h"

#define SET_A_COUNTER(A) (AB_LATCH[0] = (A << 2) | 0x1)
#define SET_B_COUNTER(B) (AB_LATCH[1] = B & 0x00FF)			// will only work with 8bit values of B

uint8_t FN_LATCH[3], R_LATCH[3], AB_LATCH[3];

void PLL_init(void)
{
	// Init USI for SPI mode
	P1SEL 		|= SCLK | SDO;		// enable SCLK and SDO as pin outputs
	P1SEL2		&= ~(SCLK | SDO);	// enable SCLK and SDO as pin outputs, too.
	USICTL0 	|= USIPE6 | USIPE5 | USIMST | USIOE;	// SDO & SCLK as outputs, master mode
	USICKCTL	&= ~USICKPL;		// clock inactive state is low
	USICTL1		|= USICKPH;			// data captured on first SCLK edge (rise)
	USICKCTL 	|= USISSEL_2;		// use SMCLK (1MHz DCO)
	USICTL0 	&= ~USISWRST;		// release for operation

	PLL_PDIR |= (CE | LE);	// set CE and LE as outputs

	LE_HI;	// LE is active low, so set as inactive

	// Function latch
	//		P = 64, current setting 2 = 5.0mA, current setting 1 = 2.5mA,
	//		Timeout = 31 pfd cycles, Fastlock mode 2, CP enabled, PD polarity = positive
	//		muxout = digital lock detect,
	//
	// R latch
	// 		R = 10, Anti-backlash width = 2.9ns, Precision = five cycles
	//		of phase less than 15ns before lock detect is set
	//
	// AB latch
	//		CP gain  = 1 (required for fastlock mode 2), B = 89, A = 55
	//
	// With these settings (R=10, P=64, B=90, A=40), initial output = 5,800MHz

	FN_LATCH[2] = 0xDD;		FN_LATCH[1] = 0xBE;		FN_LATCH[0] = 0x92;
	R_LATCH[2] = 0x10; 		R_LATCH[1] = 0x00;		R_LATCH[0] = 0x50;
	AB_LATCH[2] = 0x20;		AB_LATCH[1] = 0x59;		AB_LATCH[0] = 0xDD;

	SET_B_COUNTER(90);
	SET_A_COUNTER(40);

	// here we perform initialization using the CE pin method
	CE_LO;							// shutdown
	PLL_write_latch(&FN_LATCH);		// send function latch
	PLL_write_latch(&R_LATCH);		// send reference counter latch
	PLL_write_latch(&AB_LATCH);		// send A & B counter latch
	CE_HI;							// turn on

}

void PLL_write_latch(uint8_t (*latch)[3])
{
	uint8_t i;

	LE_LO;

	for (i=3; i>0; i--) {
		USISRL = (*latch)[i-1];			// load byte
		USICNT = 8;						// tx 8 bits
		while (!(USICTL1 & USIIFG)); 	// wait til tx is done
	}

	LE_HI;
}


// This will drive the PLL starting at 5.8 GHz, extending upwards
// and downwards to 5.85 GHz and 5.751 GHz respectively
void PLL_step_freq_outward(void)
{
	static const uint16_t Bstart = 90, Astart = 40;
	static int8_t nextStep = 1;
	static uint16_t channelCount = 0;
	static uint16_t Bup = 90, Bdown = 90;
	static uint8_t Aup = 40, Adown = 40;

	if (channelCount >= 100) {
		// We went outwards as far as possible, start
		// from middle of freq range again
		Aup = Adown = Astart;
		Bup = Bdown = Bstart;
		channelCount = 0;
		nextStep = 1;
	}

	PLL_write_latch(&AB_LATCH);

	if (nextStep == -1) {
		if (Adown == 0) {
			Adown = 63; Bdown--;
		} else {
			Adown--;
		}

		SET_B_COUNTER(Bdown);
		SET_A_COUNTER(Adown);

		nextStep = 1;		// we just went down 1 freq step

	} else {
		if (Aup == 63) {
			Aup = 0; Bup++;
		} else {
			Aup++;
		}

		SET_B_COUNTER(Bup);
		SET_A_COUNTER(Aup);

		nextStep = -1;		// we just went up 1 freq step

	}

	channelCount++;
}
